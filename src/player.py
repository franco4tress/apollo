import sys
import gi
import html
import threading
import time

gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gst, GdkPixbuf
Gst.init(sys.argv[1:])

from threading import Thread

from tinytag import TinyTag

from .utils import Utils
from urllib.request import urlopen

@Gtk.Template(resource_path='/com/fortress/apollo/player.ui')
class Player(Gtk.Box):
    __gtype_name__ = 'Player'

    go_songs_playlist = Gtk.Template.Child("go-songs-playlist")
    play_pause_button = Gtk.Template.Child("media-playback-start")
    skip_forward = Gtk.Template.Child("media-skip-forward")
    skip_backward = Gtk.Template.Child("media-skip-backward")
    skip_backwards_10 = Gtk.Template.Child("skip-backwards-10")
    skip_forward_10 = Gtk.Template.Child("skip-forward-10")
    playlist_shuffle = Gtk.Template.Child("media-playlist-shuffle")
    song_repeat = Gtk.Template.Child("media-song-repeat")
    time_progressed = Gtk.Template.Child("time-progressed")
    time_left = Gtk.Template.Child("time-left")

    song_title_label = Gtk.Template.Child("song_title_label")
    song_artist_label = Gtk.Template.Child("song_artist_label")
    song_album_label = Gtk.Template.Child("song_album_label")

    progress_bar = Gtk.Template.Child("progress-bar")
    cover_image = Gtk.Template.Child("album-cover")

    def __init__(self, **kwargs):
    	super().__init__(**kwargs)

    	self.player = self.createPlayer()
    	self.pause = False
    	self.loading_done = False
    	self.repeat_song = False
    	self.shuffle = False
    	self.duration = 0.0
    	self.progress_handler = None
    	self.keepMonitoring = True
    	self.last_song = None

    	self.go_songs_playlist.connect("clicked", self.navigate_player_page, None)

    	self.play_pause_button.connect("clicked", self.play_pause_clicked, None)
    	self.skip_forward.connect("clicked", self.skip_song_clicked, 1)
    	self.skip_backward.connect("clicked", self.skip_song_clicked, -1)
    	self.skip_backwards_10.connect("clicked", self.skip_backwards_10_clicked, None)
    	self.skip_forward_10.connect("clicked", self.skip_forward_10_clicked, None)
    	self.playlist_shuffle.connect("clicked", self.playlist_shuffle_clicked)
    	self.song_repeat.connect("clicked", self.song_repeat_clicked)

    	self.progress_handler = threading.Timer(1.0, self.handle_song_progress)
    	self.progress_handler.start()


    def do_activate(self):
    	print("Player.do_activate")


    def set_main_leaflet(self, leaflet):
    	self.leaflet = leaflet


    def set_playlist(self, playlist):
    	self.playlist = playlist


    def set_song_info(self, song_info):
        self.song_info = song_info


    def navigate_player_page(self, button, page):
        self.leaflet.set_visible_child_name("song-list-page")


    def skip_backwards_10_clicked(self, button, place_holder):
        self.move_player(-10)


    def skip_forward_10_clicked(self, button, place_holder):
        self.move_player(10)


    def move_player(self, value):
        _, current = self.player.query_position(Gst.Format.TIME)
        move_to = current + (value * 1000000000)
        if move_to < 0:
            move_to = 0
        elif move_to > self.duration:
            move_to = self.duration
        self.player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, move_to)


    def play_pause_clicked(self, button, place_holder):
        if (self.player.get_state(0).state == Gst.State.NULL):
            song = self.playlist.get_next_song(self.repeat_song, self.shuffle, 1)
            self.play_pause_button.set_icon_name("media-playback-pause-symbolic")

        elif (self.player.get_state(0).state == Gst.State.PAUSED):
            self.player.set_state(Gst.State.PLAYING)
            self.play_pause_button.set_icon_name("media-playback-pause-symbolic")

        else:
            self.player.set_state(Gst.State.PAUSED)
            self.play_pause_button.set_icon_name("media-playback-start-symbolic")


    def skip_song_clicked(self, button, delta):
        current_song = self.playlist.playlist_songs.get_selected_row()
        if current_song != None and current_song.get_index() in self.playlist.filtered_songs and self.repeat_song:
            self.player.set_state(Gst.State.NULL)
            self.player.set_state(Gst.State.PLAYING)
            self.play_pause_button.set_icon_name("media-playback-pause-symbolic")

        else:
            song = self.playlist.get_next_song(self.repeat_song, self.shuffle, delta) # 1 means next song, -1 means previous song.


    def playlist_shuffle_clicked(self, place_holder):
        self.shuffle = not self.shuffle


    def song_repeat_clicked(self, place_holder):
        self.repeat_song = not self.repeat_song


    def createPlayer(self):
        Gst.init(None)
        player = Gst.ElementFactory.make("playbin", "player")
        fakesink = Gst.ElementFactory.make("fakesink", "fakesink")
        player.set_property("video-sink", fakesink)
        bus = player.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message)

        return player


    def stop_player(self):
        self.player.set_state(Gst.State.NULL)
        self.progress_handler.cancel()
        self.updating_song_progress.cancel()
        self.keepMonitoring = False
        self.player = None


    def play_song(self, artist, title, album, image, filepath, row):
        self.player.set_state(Gst.State.NULL)
        self.player.set_property('uri', 'file://' + filepath)
        self.player.set_state(Gst.State.PLAYING)
        self.play_pause_button.set_icon_name("media-playback-pause-symbolic")
        self.time_progressed.set_label("0:00")
        self.time_left.set_label("-{}".format(row.time))
        if self.last_song != row:
             self.display_song_info(artist, title, album, image, row)

        self.last_song = row


    def display_song_info(self, artist, title, album, image, row):
        self.song_title_label.set_label(title)
        self.song_artist_label.set_label(artist)
        self.song_album_label.set_label(album)
        if image != None:
            if row.image_is_pixbuf:
                self.cover_image.set_from_pixbuf(image)

            else:
                self.cover_image.set_from_file(image)

        else:
            self.retrieve_album_image(row)

        if row.lyric == None:
            Thread(group=None, target=self.retrieve_song_lyrics, args=(artist, title, row,)).start()

        else:
            self.song_info.display_lyrics(row.lyric)


    def retrieve_album_image(self, row):
        metadata = TinyTag.get(row.filepath, image=True)
        if not metadata.get_image():
            Thread(group=None, target=self.retrieve_album_image_from_url, args=(metadata.artist, metadata.album, row,)).start()

        else:
            loader = GdkPixbuf.PixbufLoader()
            loader.write(metadata.get_image())
            loader.close()
            loader.set_size(200,200)
            self.cover_image.set_from_pixbuf(loader.get_pixbuf())


    def retrieve_album_image_from_url(self, artist, album, row):
        self.cover_image.set_from_icon_name("folder-music-symbolic")
        url = row.image_url
        if url == None:
            url = Utils.get_cover_art(artist, album, row)

        row.image_url = url
        response = urlopen(url)
        loader = GdkPixbuf.PixbufLoader()
        loader.write(response.read())
        loader.close()
        loader.set_size(200,200)
        self.cover_image.set_from_pixbuf(loader.get_pixbuf())


    def retrieve_song_lyrics(self, artist, title, row):
        lyric = Utils.get_song_lyrics(artist, title)
        row.lyric = lyric
        self.song_info.display_lyrics(row.lyric)


    def on_message(self, bus, message):
        t = message.type
        if t == Gst.MessageType.EOS:
            if self.repeat_song:
                self.player.set_state(Gst.State.NULL)
                self.player.set_state(Gst.State.PLAYING)
            else:
                next_song = self.playlist.get_next_song(self.repeat_song, self.shuffle, 1)

        elif t == Gst.MessageType.ERROR:
            self.player.set_state(Gst.State.NULL)
            err, debug = message.parse_error()
            print("Error: %s" % err, debug)

        elif t == Gst.MessageType.DURATION_CHANGED:
            self.duration = Gst.CLOCK_TIME_NONE


    def update_song_progress(self):
        while self.keepMonitoring:
            time.sleep(0.25)
            if self.player != None and self.player.query_position(Gst.Format.TIME)[0]:
                progress = int(self.player.query_position(Gst.Format.TIME)[1] / 1000000000)
                progress = int(str(progress).split(".")[0])
                if self.loading_done and self.playlist.playlist_songs.get_selected_row() != None:
                    time_left = int((self.playlist.minutes_to_seconds(self.playlist.playlist_songs.get_selected_row().time))) - progress
                    self.time_progressed.set_label(self.playlist.seconds_to_minutes(progress))
                    self.time_left.set_label("-{}".format(self.playlist.seconds_to_minutes(time_left)))


    def handle_song_progress(self):
        try:
            # listen to the bus
            bus = self.player.get_bus()
            self.updating_song_progress = threading.Timer(1.0, self.update_song_progress)
            self.updating_song_progress.start()
            while self.keepMonitoring:
                time.sleep(0.5)
                msg = bus.timed_pop_filtered(
                    100 * Gst.MSECOND,
                    (Gst.MessageType.STATE_CHANGED | Gst.MessageType.ERROR | Gst.MessageType.EOS | Gst.MessageType.DURATION_CHANGED)
                )

                # parse message
                if msg:
                    self.on_message(bus, msg)
                    # we got no message. this means the timeout expired
                if self.player:
                    current = -1
                    # query the current position of the stream
                    ret, current = self.player.query_position(Gst.Format.TIME)
                    # if not ret:
                    #     print("ERROR: Could not query current position")

                    # if we don't know it yet, query the stream duration
                    if self.duration == Gst.CLOCK_TIME_NONE:
                        (ret, self.duration) = self.player.query_duration(Gst.Format.TIME)
                        # if not ret:
                        #    print("ERROR: Could not query stream duration")

                    if self.duration != 0.0:
                        self.progress_bar.set_fraction(current / self.duration)

            self.progress_bar.set_fraction(0.0)

        finally:
            if self.player != None:
                self.player.set_state(Gst.State.NULL)




