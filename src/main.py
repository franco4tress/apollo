# main.py
#
# Copyright 2022 Franco Navarro
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import gi

gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gio, Gdk, GLib

gi.require_version('Adw', '1')
from gi.repository import Adw
Adw.init()

from .window import ApolloWindow

class ApolloApplication(Gtk.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(application_id='com.fortress.apollo', flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.create_action('quit', self.quit, ['<primary>q'])
        self.create_action('about', self.on_about_action)
        self.create_action('preferences', self.on_preferences_action)


    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win = self.props.active_window
        if not win:
            win = ApolloWindow(application=self)

        self.loadCssStyles()
        win.connect('close-request', self.on_window_main_destroy)
        win.present()


    def on_window_main_destroy(self, window, data=None):
        window.stop_player()


    def on_about_action(self, widget, _):
        """Callback for the app.about action."""
        about = Gtk.AboutDialog(transient_for=self.props.active_window,
                                modal=True,
                                program_name='apollo',
                                logo_icon_name='com.fortress.apollo',
                                version='0.5.0',
                                authors=['Franco Navarro'],
                                copyright='© 2022 Franco Navarro')
        about.present()


    def on_preferences_action(self, widget, _):
        """Callback for the app.preferences action."""
        print('app.preferences action activated')


    def create_action(self, name, callback, shortcuts=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


    def loadCssStyles(self):
        provider = Gtk.CssProvider()
        provider.load_from_resource('/com/fortress/apollo/apollo.css')
        Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


def main(version):
    """The application's entry point."""
    app = ApolloApplication()
    return app.run(sys.argv)

