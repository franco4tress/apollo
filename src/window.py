# window.py
#
# Copyright 2022 Franco Navarro
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import threading
import asyncio
import musicbrainzngs
import json

from gi.repository import Gtk
from gi.repository import Adw

from .player import Player
from .playlist import Playlist
from .info import Info

from .utils import Utils

@Gtk.Template(resource_path='/com/fortress/apollo/window.ui')
class ApolloWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'ApolloWindow'

    leaflet = Gtk.Template.Child("leaflet")
    songs_list_page = Gtk.Template.Child("song-list-page")
    player_page = Gtk.Template.Child("player-page")
    playlist = Gtk.Template.Child("play-list")
    song_info = Gtk.Template.Child("song-info")
    player = Gtk.Template.Child("player")
    toast_overlay = Gtk.Template.Child("song-filter-box")
    go_next_symbolic = Gtk.Template.Child("go-next-symbolic")
    playlist_tools = Gtk.Template.Child("playlist-tools")
    playlist_search = Gtk.Template.Child("playlist-search")
    playlist_pin_search = Gtk.Template.Child("pin-search")
    open_playlist_filters = Gtk.Template.Child("song-filter")
    playlist_filters_box = Gtk.Template.Child("playlist-filters")
    playlist_filters_container = Gtk.Template.Child("playlist-filters-container")
    select_folder = Gtk.Template.Child("select-folder")
    select_file = Gtk.Template.Child("select-file")
    autodiscover = Gtk.Template.Child("autodiscover")
    music_path_select = Gtk.Template.Child("music-path-select")
    remove_custom_filters_button = Gtk.Template.Child("remove-custom-filters")


    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.app_running = True
        self.filters_enabled = False
        self.path = None
        self.removing_filters = False
        self.custom_filters = 0
        self.album_years = []
        # self.custom_filters = []
        self.active_custom_filters = []
        self.leaflet.set_visible_child_name("player-page")
        self.playlist.set_window(self)
        self.player.set_main_leaflet(self.leaflet)
        self.player.set_playlist(self.playlist)
        self.playlist.set_player(self.player)
        self.player.set_song_info(self.song_info)
        self.playlist_pin_search.set_sensitive(False)
        self.remove_custom_filters_button.set_sensitive(False)
        self.go_next_symbolic.connect("clicked", self.navigate_player_page, None)
        self.playlist_search.connect("search-changed", self.handle_filter, None)
        self.playlist_pin_search.connect("clicked", self.pin_search, None)
        self.open_playlist_filters.connect("clicked", self.open_filters_box, None)
        self.autodiscover.connect("clicked", self.get_music_paths, 1)
        self.select_folder.connect("clicked", self.get_music_paths, 2)
        self.select_file.connect("clicked", self.get_music_paths, 3)
        self.remove_custom_filters_button.connect("clicked", self.offer_filter_removal, None)

        self.music_loder_handler = None


    def offer_filter_removal(self, button, _):
        if button.get_active():
            self.removing_filters = True

        else:
            self.removing_filters = False

        for song_filter in self.playlist_filters_box:
            song_filter = song_filter.get_child()
            if hasattr(song_filter, 'category') and song_filter.category == "custom":
                if button.get_active():
                    song_filter.set_name("custom-remove")

                else:
                    song_filter.set_name("plain")

            else:
                if button.get_active():
                    song_filter.set_sensitive(False)

                else:
                    song_filter.set_sensitive(True)

        button.set_sensitive(True)


    def handle_filter(self, two, three):
        search_entry = self.playlist_search.get_text()
        if search_entry == "":
            self.playlist_pin_search.set_sensitive(False)

        else:
            self.playlist_pin_search.set_sensitive(True)

        (shown_songs, shown_indexes) = self.playlist.filter_playlist_songs(search_entry)
        count = -1
        for song in shown_songs:
            count = count + 1

        self.playlist.filter_songs(shown_songs, shown_indexes)


    def load_json_data(self, json_data):
        songs_found = []
        songs_not_found = []
        # Defining year and genre lists.
        years = []
        genres = []

        for data in json_data:
            for row in self.playlist.playlist_songs:
                if row.title == data.get("title") and row.artist == data.get("artist") and row.genre == data.get("genre"):
                    row.year = data.get("year")
                    # Adding years and genres to lists.
                    years.append(row.year)
                    genres.append(row.genre)

                    if row.year == None:
                        row.year = 99999999

                    else:
                        row.year = int(row.year)

                    row.image_url = data.get("image")
                    row.lyric = data.get("lyric")
                    songs_found.append(row)

        # Finding decades and genres for filters.
        decades = self.find_decades(years)
        unique_genres = self.find_genres(genres)

        # Generating preset filters.
        self.generate_preset_filters(unique_genres, decades)

        if songs_found:
            for row in self.playlist.playlist_songs:
                if row not in songs_found:
                    songs_not_found.append(row)

            if songs_not_found != []:
                self.playlist_filters_box.set_sensitive(False)
                self.music_metadata(songs_not_found, False)
                for row in songs_not_found:
                    json_data.append({"title": row.title, "artist": row.artist, "genre": row.genre, "year": row.year, "image": row.image_url, "lyric": row.lyric})
                    self.save_metadata(json_data)
                    self.playlist_filters_box.set_sensitive(True)

        else:
            self.playlist_filters_box.set_sensitive(False)
            self.music_metadata(self.playlist.playlist_songs, False)
            for row in self.playlist.playlist_songs:
                json_data.append({"title": row.title, "artist": row.artist, "genre": row.genre, "year": row.year, "image": row.image_url, "lyric": row.lyric})
                self.save_metadata(json_data)
                self.playlist_filters_box.set_sensitive(True)


    def find_genres(self, genres):
        unique_genres = []
        for genre in genres:
            if genre not in unique_genres:
                if genre == "":
                    unique_genres.append("Unknown")
                else:
                    unique_genres.append(genre)

        unique_genres.sort()
        return unique_genres


    def find_decades(self, years):
        decades = []
        for year in years:
            if year == 999999 and "Unknown" not in decades:
                decades.append("Unknown")
            elif year != 999999:
                decade = str(year)[:-1] + "0s"
                if decade not in decades:
                    decades.append(decade)

        decades.sort()
        return decades


    def validate_path(self, path, create=True):
        if not os.path.exists(path) and create:
            os.makedirs(path)
        return True


    def get_music_paths(self, button, var):
        if var == 1:
            self.path = Utils.get_music_paths()[0]
            self.start_loading_music([self.path], 1)

        elif var == 2:
            filechooserdialog = Gtk.FileChooserDialog(title="Open...",
                         action=Gtk.FileChooserAction.SELECT_FOLDER)
            self.select_music(filechooserdialog, 2)

        else:
            filechooserdialog = Gtk.FileChooserDialog(title="Open...",
                         action=Gtk.FileChooserAction.OPEN)
            self.select_music(filechooserdialog, 3)


    def start_loading_music(self, files, func):
        self.music_path_select.set_visible(False)
        self.playlist_tools.set_visible(True)
        self.player.skip_forward.set_sensitive(True)
        self.player.skip_backward.set_sensitive(True)
        self.player.play_pause_button.set_sensitive(True)
        self.music_loder_handler = threading.Timer(1.0, self.load_music, [files, func])
        self.music_loder_handler.start()


    def get_filepath_from_giofile(self, file):
        return file.get_path()


    def load_music(self, files, func):
        for file in files:
            if func != 3:
                self.playlist.load_songs(file)

            else:
                self.playlist.load_file(file)

        self.validate_path(".local/share/apollo/")
        if os.path.exists(".local/share/apollo/.apollo.json"):
            self.filters_enabled = True
            json_file = open(".local/share/apollo/.apollo.json")
            json_data = json.load(json_file)
            self.load_json_data(json_data)


    def select_music(self, filechooserdialog, func):
        filechooserdialog.set_select_multiple(True)
        filechooserdialog.add_buttons("_Open", Gtk.ResponseType.OK)
        filechooserdialog.add_buttons("_Cancel", Gtk.ResponseType.CANCEL)
        filechooserdialog.set_default_response(Gtk.ResponseType.OK)
        filechooserdialog.set_transient_for(self)

        filechooserdialog.connect("response", self.directory, func)
        filechooserdialog.show()


    def directory(self, filechooserdialog, response, func):
        filepaths = []
        if response == Gtk.ResponseType.OK:
            giofiles = filechooserdialog.get_files()
            filepaths = list(map(self.get_filepath_from_giofile, giofiles))
            if filepaths:
                self.start_loading_music(filepaths, func)

        filechooserdialog.destroy()
        count = -1
        for filepath in filepaths:
            count += 1
            self.playlist.filtered_songs.append(count)


    def stop_player(self):
        self.player.stop_player()
        self.playlist.app_running = False
        self.app_running = False


    def do_activate(self):
    	print("ApolloWindow.do_activate")


    def navigate_player_page(self, button, page):
        self.leaflet.set_visible_child_name("player-page")


    def pin_search(self, button, _):
        new_button = self.create_filter_button(self.playlist_search.get_text(), self.custom_filter_update, "custom")
        self.playlist_search.set_text("")
        new_button.set_active(True)
        self.custom_filter_update(new_button, new_button.get_label())
        self.custom_filters += 1
        if self.custom_filters == 1:
            self.remove_custom_filters_button.set_sensitive(True)


    def create_filter_button(self, text, connection, category):
        new_button = Gtk.ToggleButton()
        new_button.set_label(text)
        new_button.connect("clicked", connection, text)
        new_button.add_css_class("circular")
        new_button.set_margin_start(5)
        new_button.set_margin_top(5)
        new_button.category = category
        new_button.set_size_request(100, 5)
        new_button.set_valign(Gtk.Align.START)
        self.playlist_filters_box.append(new_button)

        return new_button


    def custom_filter_update(self, button, text):
        if self.removing_filters:
            self.playlist_filters_box.remove(button)
            self.custom_filters -= 1
            if self.custom_filters == 0:
                self.remove_custom_filters_button.set_active(False)
                self.removing_filters = False
                for song_filter in self.playlist_filters_box:
                    song_filter = song_filter.get_child()
                    song_filter.set_sensitive(True)

                self.remove_custom_filters_button.set_sensitive(False)

        self.handle_filter(None, None)


    def open_filters_box(self, button, _):
        if self.filters_enabled:
            if button.get_icon_name() == "up-symbolic":
                self.playlist_filters_box.show()
                button.set_icon_name("down-symbolic")
                self.playlist_filters_container.show()

            else:
                self.playlist_filters_box.hide()
                button.set_icon_name("up-symbolic")
                self.playlist_filters_container.hide()

        else:
            self.new_toast(self.toast_overlay, "The app has to search for song data.", True, "Allow", 3, self.allow_collect_metadata)


    def matches_filter(self, row):
        row_labels = [row.title.upper(), row.artist.upper(), row.album.upper()]
        active_filters = 0
        custom = 0
        active_custom = 0
        decade = 0
        active_decade = 0
        genre = 0
        active_genre = 0
        for playlist_filter in self.playlist_filters_box:
            # TODO: Clean data before use in logic downstream to ensure concise and clear code with little data manipulation at use sites.
            song_filter = playlist_filter.get_child()
            if song_filter.get_active():
                filter_label = song_filter.get_label()
                active_filters += 1
                if hasattr(song_filter, 'category') and song_filter.category == "custom":
                    active_custom += 1
                    for label in row_labels:
                        if filter_label.upper() in label:
                            custom += 1

                else:
                    if hasattr(song_filter, 'category') and song_filter.category == "decade":
                        active_decade += 1
                        label_is_int = Utils.is_int(filter_label.strip("s"))
                        if (not label_is_int and row.year == 999999) or (label_is_int
                            and str(row.year)[:-1] == filter_label[:-2]):
                                decade += 1

                    elif hasattr(song_filter, 'category') and song_filter.category == "genre":
                        active_genre += 1
                        if (row.genre != None and filter_label.lower() == row.genre.lower()) or (row.genre == "" and filter_label == "Unknown"):
                            genre += 1

        tags = [active_custom, active_decade, active_genre]
        count = 0
        for tag in tags:
            count += 1
            if tag == 0:
                if count == 1:
                    custom = 1

                elif count == 2:
                    decade = 1

                elif count == 3:
                    genre = 1

        if custom >= 1 and decade >= 1 and genre >= 1:
            return True

        if active_filters == 0:
            return True

        return False


    def new_toast(self, overlay, label, display_button=False, button_label=None, timeout=2, clicked=None):
        toast = Adw.Toast.new(label)
        toast.set_timeout(timeout)
        if display_button:
            toast.set_button_label(button_label)

        if clicked != None:
            toast.connect("button-clicked", clicked, None)

        overlay.add_toast(toast)


    def allow_collect_metadata(self, two, three):
        self.open_playlist_filters.set_sensitive(False)
        self.collect_metadata = threading.Timer(1.0, self.call_music_metadata)
        self.collect_metadata.start()


    def call_music_metadata(self):
        self.music_metadata(self.playlist.playlist_songs)


    def music_metadata(self, playlist, save_toast=True):
        # Defining lists for years and genres.
        years = []
        genres = []

        years_found = 0
        last_percentage = -1
        if playlist == self.playlist.playlist_songs:
            playlist_len = self.playlist.playlist_songs.get_last_child().get_index() + 1
        else:
            playlist_len = len(playlist)

        for row in playlist:
            if self.app_running == False:
                break

            releases = musicbrainzngs.search_releases(artist=row.artist, release=row.title, strict=True, limit=25)
            year = Utils.get_release_year(enumerate(releases['release-list']))
            row.year = int(year)
            found = False
            for album_year in self.album_years:
                if "{} - {}".format(row.artist, row.album) == album_year[0]:
                    if row.year <= album_year[1]:
                        album_year[1] = row.year
                        found = True

                    else:
                        row.year = album_year[1]
                        found = True

            if self.album_years == None or found == False:
                self.album_years.append(["{} - {}".format(row.artist, row.album), row.year])

            years_found += 1
            percentage = int(str(years_found / playlist_len * 100).split(".")[0])

            if last_percentage != percentage:
                last_percentage += 1
                self.new_toast(self.toast_overlay, "{}%".format(percentage))

        for row in playlist:
            for album_year in self.album_years:
                if "{} - {}".format(row.artist, row.album) == album_year[0]:
                    row.year = album_year[1]

            # Adding year and genre to appropriate lists.
            years.append(row.year)
            genres.append(row.genre)

            # Finding decades and genres for filters.
            decades = self.find_decades(years)
            unique_genres = self.find_genres(genres)

            # Generating preset filters.
            self.generate_preset_filters(unique_genres, decades)

        self.new_toast(self.toast_overlay, "Done")
        self.open_playlist_filters.set_sensitive(True)
        self.filters_enabled = True
        if save_toast:
            self.new_toast(self.toast_overlay, "This data can be saved to your device for later use.", True, "Allow", 5, self.allow_save_metadata)


    def generate_preset_filters(self, genres, decades):
        for genre in genres:
            _ = self.create_filter_button(genre, self.handle_filter, "genre")

        for decade in decades:
            _ = self.create_filter_button(decade, self.handle_filter, "decade")


    def allow_save_metadata(self, two, three):
        self.save_metadata_file = threading.Timer(1.0, self.save_metadata)
        self.save_metadata_file.start()


    def save_metadata(self, text_to_save=[]):
        if text_to_save == []:
            for row in self.playlist.playlist_songs:
                if self.app_running == False:
                    break

                text_to_save.append({"title": row.title, "artist": row.artist, "genre": row.genre, "year": row.year, "image": row.image_url, "lyric": row.lyric})

        json_text = json.dumps(text_to_save, indent=4)
        json_file = open(".local/share/apollo/.apollo.json", "w")
        json_file.write(json_text)
        json_file.close()

