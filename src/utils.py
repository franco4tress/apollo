# -*- coding: utf-8 -*-

# SPDX-License-Identifier: GPL-3.0+
# Author: Franco Navarro <franco4tress@protonmail.com>

import musicbrainzngs
import bs4
import json

import urllib3

from gi.repository import GLib

musicbrainzngs.set_useragent(
    "com.fortress.apollo",
    "0.5",
    "https://gitlab.com/franco4tress/apollo",
)

class Utils:

    # TODO: Create a new Utils library for non-webscraper functions.
    def is_int(val):
        try:
            int(val)
        except ValueError:
            return False
        else:
            return True


    def get_music_paths():
        if GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC):
            paths = [GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC)]
        else:
            print("You need to add a music path to Apollo Mobile")

        return paths


    def get_release_year(releases):
        release_date = 999999
        for (idx, release) in releases:
            if 'date' in release and release['date'] != "":
                year = release['date'].split("-")
                year = int(year[0])
                if year < release_date:
                    release_date = year

        return release_date


    def get_cover_art(artist, album, row):
        urls = []
        release_ids = []
        result = musicbrainzngs.search_releases(artist=artist, release=album, limit=25)

        if not result['release-list']:
            return

        for (idx, release) in enumerate(result['release-list']):
            release_ids.append(release['id'])

        http = urllib3.PoolManager()

        for release_id in release_ids:
            response = http.request("GET", "https://musicbrainz.org/release/{}/cover-art".format(release_id))
            if response is not None:
                try:
                    html = bs4.BeautifulSoup(response.data, 'html.parser')
                    image = html.find_all("span", class_="cover-art-image")
                    url = image[0]["data-small-thumbnail"]
                    urls.append('https:' + url)
                    break

                except Exception:
                    print("Error fetching album cover.")

        if len(urls) != 0:
            return urls[0]

        else:
            return None


    def get_song_lyrics(artist, title):
        lyrics = "Sorry, we couldn't find any lyrics for \n{}.".format(title)
        http = urllib3.PoolManager()

        response = http.request("GET", "https://songlyrics.com/{}/{}-lyrics".format(artist, title))
        if response is not None:
            html = bs4.BeautifulSoup(response.data, 'html.parser')
            found_lyrics = str(html.find_all("p", class_="songLyricsV14 iComment-text"))
            if "Sorry, we have no {} - {} lyrics at the moment.".format(artist, title) not in found_lyrics:
                found_lyrics = found_lyrics.replace("<br/>", "")
                found_lyrics = found_lyrics.strip('[<p class="songLyricsV14 iComment-text" id="songLyricsDiv">')
                found_lyrics = found_lyrics.strip("</p>]")
                lyrics = found_lyrics

            if lyrics == None:
                print("Error fetching song lyrics.")


        return lyrics

