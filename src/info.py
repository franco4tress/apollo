from gi.repository import Gtk
from gi.repository import Adw

@Gtk.Template(resource_path='/com/fortress/apollo/info.ui')
class Info(Gtk.ScrolledWindow):
    __gtype_name__ = 'Info'

    player = None
    info_label = Gtk.Template.Child("info-songs")
    info_label_courtesy = Gtk.Template.Child("info-songs-courtesy")

    def __init__(self, **kwargs):
    	super().__init__(**kwargs)


    def do_activate(self):
    	print("Info.do_activate")


    def display_lyrics(self, lyrics):
        self.info_label.set_text(lyrics)
        self.info_label_courtesy.set_text("Lyrics courtesy of Songlyrics.com")


    def set_player(self, player):
        self.player = player
