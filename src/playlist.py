import os
import html
import random
import musicbrainzngs
import threading
import datetime

from gi.repository import GdkPixbuf
from gi.repository import Gtk
from gi.repository import Adw
from gi.repository import GLib

from tinytag import TinyTag

from .utils import Utils

@Gtk.Template(resource_path='/com/fortress/apollo/playlist.ui')
class Playlist(Gtk.ScrolledWindow):
    __gtype_name__ = 'Playlist'

    player = None
    playlist_songs = Gtk.Template.Child("playlist-songs")


    def __init__(self, **kwargs):
    	super().__init__(**kwargs)
    	self.search_results = []
    	self.filter_results = []
    	self.filters_applied = []
    	self.app_running = True
    	self.playing_image = None
    	self.previous_song = None
    	self.filtered_songs = []
    	self.selected_index = 0
    	self.playlist_songs.connect("selected-rows-changed", self.song_selected, None)
    	self.audio_extensions = {".mp3", ".flac", ".mp4", ".wav", ".wave", ".ogg", ".opus", ".wma", ".m4a", ".m4b"}


    def do_activate(self):
    	print("Playlist.do_activate")


    def find_filtered_song_index(self, song_index):
        count = -1
        for index in self.filtered_songs:
            count = count + 1
            if song_index == index:
                return count


    def song_selected(self, listbox, _):
        current_song = listbox.get_selected_row()
        self.selected_index = self.find_filtered_song_index(current_song.get_index())
        if self.previous_song != None and self.playing_image != None:
            self.previous_song.remove(self.playing_image)

        self.playing_image = Gtk.Image()
        self.playing_image.set_size_request(32, 32)
        self.playing_image.set_from_icon_name("audio-only-symbolic")
        current_song.add_suffix(self.playing_image)
        self.previous_song = current_song

        self.player.play_song(current_song.artist, current_song.title, current_song.album, current_song.image, current_song.filepath, current_song)


    def minutes_to_seconds(self, length):
        (minutes, seconds) = str(length).split(":")

        return int(minutes) * 60 + int(seconds)


    def seconds_to_minutes(self, length):
        duration = str(datetime.timedelta(seconds=length))
        duration = duration.split(":")
        duration.pop(0)
        seconds = str(round(float(duration[1]), 0)).split(".")
        if int(seconds[0]) == 60:
            duration[1] = "00"
            duration[0] = str(int(duration[0]) + 1)

        else:
            duration[1] = seconds[0]

        if duration[0][0] == "0":
            duration[0] = duration[0][1:]

        if len(duration[1]) == 1:
            duration[1] = "0" + duration[1]

        duration = ":".join(duration)

        return duration


    def load_file(self, file_path):
        self.set_visible(True)
        self.window.toast_overlay.set_visible(True)
        _, extension = os.path.splitext(file_path)
        song = self.find_metadata(file_path, extension)
        self._add_row(self.playlist_songs, song[0], song[1], song[2], song[3], song[4], song[5], None, None)
        self.player.loading_done = True


    def find_metadata(self, file_path, extension):
        if extension in self.audio_extensions:
            metadata = TinyTag.get(file_path, image=True)
            title = None
            artist = None
            album = None
            genre = None
            time = None
            try:
                title = metadata.title
                artist = metadata.artist
                album = metadata.album
                genre = metadata.genre
                time = self.seconds_to_minutes(metadata.duration)

            except Exception:
                if title == None:
                    title = os.path.basename(file_path).strip(extension)

                if artist == None:
                    artist = "Unknown"

                if album == None:
                    album = "Unknown"

                if time == None:
                    time = "Unknown"

                if genre == None:
                    genre = "Unknown"

            return [title, artist, album, file_path, time, genre]


    def load_songs(self, root_path):
        self.set_visible(True)
        self.window.toast_overlay.set_visible(True)
        image_extensions = {".jpg", ".jpeg", ".png", ".gif"}
        songs = []
        images = []
        for root, subdirs, files in os.walk(root_path):
            image = None
            for file in files:
                file_path = os.path.join(root, file)
                _, extension = os.path.splitext(file_path)
                if extension.lower() in image_extensions:
                    if image != None:
                        if "front" in file_path:
                            image = file_path
                    else:
                        image = file_path

                elif extension in self.audio_extensions:
                    songs.append(self.find_metadata(file_path, extension))

                if self.app_running == False:
                    break

            if image != None:
                images.append(image)

        self.filtered_songs = []
        count = -1
        for song in songs:
            count += 1
            img = self.find_image(images, song[3])
            self._add_row(self.playlist_songs, song[0], song[1], song[2], song[3], song[4], song[5], img, None)
            self.filtered_songs.append(count)

        self.player.loading_done = True


    def find_image(self, images, file_path):
        song_path = os.path.dirname(file_path)
        for image in images:
            if song_path in os.path.dirname(image):
                return image
        return None


    def filter_playlist_songs(self, entry):
        self.filtered_songs = []
        self.search_results = []
        count = -1
        for row in self.playlist_songs:
            count += 1
            tags = [row.title.upper(),row.artist.upper(),row.album.upper()]
            found = False
            for tag in tags:
                if entry.upper() in tag:
                    row.show()
                    self.filtered_songs.append(count)
                    self.search_results.append(row)
                    found = True

            if not found:
                row.hide()

        return (self.search_results, self.filtered_songs) # condense this function


    def filter_songs(self, shown_songs, shown_indexes):
        count = -1
        difference = 0
        for row in shown_songs:
            count = count + 1
            if not self.window.matches_filter(row):
                row.hide()
                shown_indexes.pop(count - difference)
                difference = difference + 1


    def _add_row(self, listbox, name, desc, album, file_path, time, genre, image, clicked):
        row = Adw.ActionRow(name="song")
        time_label = Gtk.Label()
        time_label.set_label(time)
        time_label.set_name("time_label")

        row.set_title(html.escape(name))
        row.set_subtitle(html.escape(desc) + " - " + html.escape(album))
        row.add_suffix(time_label)

        row.filepath = file_path
        row.artist = desc
        row.title = name
        row.album = album
        row.time = time
        row.genre = genre
        row.image = image
        row.image_is_pixbuf = False
        row.year = None
        row.lyric = None
        row.image_url = None

        image_to_set = Gtk.Image()
        image_to_set.set_size_request(36, 36)
        image_to_set.set_margin_top(10)
        image_to_set.set_margin_bottom(10)

        metadata = TinyTag.get(row.filepath, image=True)

        if metadata.get_image() != None:
            loader = GdkPixbuf.PixbufLoader()
            loader.write(metadata.get_image())
            loader.close()
            loader.set_size(200,200)
            row.image = loader.get_pixbuf()
            row.image_is_pixbuf = True
            image_to_set.set_from_pixbuf(row.image)

        elif row.image != None:
            image_to_set.set_from_file(image)

        else:
            image_to_set.set_from_resource("/com/fortress/apollo/icons/scalable/actions/music-note-symbolic.png")

        row.add_prefix(image_to_set)

        listbox.insert(row, -1)


    def get_next_song(self, repeat_song, shuffle, delta):
        current_song = self.playlist_songs.get_selected_row()
        if current_song != None and current_song.get_index() not in self.filtered_songs:
            self.playlist_songs.select_row(self.playlist_songs.get_row_at_index(self.filtered_songs[0]))
            selected_song = self.playlist_songs.get_selected_row()
            return selected_song

        if delta == 1:
            if shuffle == False:
                if current_song == None:
                    self.playlist_songs.select_row(self.playlist_songs.get_row_at_index(self.filtered_songs[0]))

                else:
                    if self.selected_index != len(self.filtered_songs) - 1:
                        self.selected_index = self.selected_index + 1
                        self.playlist_songs.select_row( self.playlist_songs.get_row_at_index(self.filtered_songs[self.selected_index]))

                    else:
                        self.playlist_songs.select_row(self.playlist_songs.get_row_at_index(self.filtered_songs[0]))
            else:
                self.playlist_songs.select_row(self.playlist_songs.get_row_at_index(self.filtered_songs[random.randrange(0, len(self.filtered_songs) - 1)]))

        else:
            if shuffle == False:
                if self.selected_index == 0:
                    self.selected_index = len(self.filtered_songs) - 1

                else:
                    self.selected_index = self.selected_index - 1

                self.playlist_songs.select_row(self.playlist_songs.get_row_at_index(self.filtered_songs[self.selected_index]))

            else:
                self.playlist_songs.select_row(self.playlist_songs.get_row_at_index(random.randrange(0, self.playlist_songs.get_last_child().get_index())))

        selected_song = self.playlist_songs.get_selected_row()
        return selected_song


    def set_window(self, window):
        self.window = window


    def set_player(self, player):
        self.player = player



